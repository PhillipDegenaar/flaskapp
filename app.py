import flask
import socket
app = flask.Flask(__name__)


@app.route('/', methods=['GET'])
def home():
    host_name = socket.gethostname()


    IP_address = socket.gethostbyname(host_name)

    data = {
        'hostname': host_name,
        'ip': IP_address,
        'status': 'Application up and running!!!'
    }
    return f"<h1> DEMO FLASK APP V18</h1><br><p>{data}</p>"


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")